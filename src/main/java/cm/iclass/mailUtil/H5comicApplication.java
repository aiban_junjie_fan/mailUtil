package cm.iclass.mailUtil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class H5comicApplication {

    public static void main(String[] args) {
        SpringApplication.run(H5comicApplication.class, args);
    }

}
