package cm.iclass.mailUtil.config;


import cm.iclass.mailUtil.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Description: 异常处理器
 * @Author: alan
 * @Date: 2019/6/10 5:34 PM
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 全局异常
     *
     * @param e
     * @return cn.iclass.common.utils.R
     * @Author alan
     * @Date 2019/6/10 5:35 PM
     */
    @ExceptionHandler(Exception.class)
    public R handleException(Exception e) {
        log.error(e.getMessage(), e);
        return R.error();
    }
}
