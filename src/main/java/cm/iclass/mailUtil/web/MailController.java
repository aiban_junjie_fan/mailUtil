package cm.iclass.mailUtil.web;


import cm.iclass.mailUtil.utils.FileUtil;
import cm.iclass.mailUtil.utils.MailUtil;
import cm.iclass.mailUtil.utils.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;


/**
 * @Description: MailController.java
 * @Author: alan
 * @Date: 2019/8/2 5:00 PM
 */
@RestController
@RequestMapping("mail")
public class MailController {

    /**
     * 所有书籍
     */
    @GetMapping("/send")
    public R send(
            @RequestParam(value = "from") @NotBlank String from,
            @RequestParam(value = "name") @NotBlank String name,
            @RequestParam(value = "contact") @NotBlank String contact
    ) {
/*        MailUtil.sendMail("ly@iclass.com", "商务合作",
                new StringBuffer("我是来自").append(from).append("的").append(name).append(",我的联系方式是").append(contact).append("。").toString());*/
        String content = new StringBuffer("我是来自").append(from).append("的").append(name).append(",我的联系方式是").append(contact).append("。").toString();
        FileUtil.recordMail(content);
        return R.ok();
    }
}
