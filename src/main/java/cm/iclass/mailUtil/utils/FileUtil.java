package cm.iclass.mailUtil.utils;


import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtil {
    public  static  void recordMail (String content) {
        String fileName = "/data/recordMail.txt";
        try{
            SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            FileWriter writer = new FileWriter(fileName,true);
            writer.write("\r\n");
            writer.write( dft.format(new Date())+"\r\n");
            writer.write(content+"\r\n");
            writer.close();
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        String content = new StringBuffer("我是来自").append("爱班").append("的").append("蔡浙明").append(",我的联系方式是").append("18806533161").append("。").toString();
        recordMail(content);
    }

}
