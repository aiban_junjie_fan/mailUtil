package cm.iclass.mailUtil.utils;


import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * @Description: 发送邮件
 * @Author: alan
 * @Date: 2018/12/17 10:11 AM
 */
public class MailUtil {

    public static JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.exmail.qq.com");
        mailSender.setPort(465);


        mailSender.setUsername("notice@chain-pin.com");
        mailSender.setPassword("Iclass0711");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.enable", "true");
        props.put("ssl.trust", "smtp.exmail.qq.com");
        props.put("mail.debug", "true");

        return mailSender;
    }

    public static void sendMail(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("通知 <notice@chain-pin.com>");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        getJavaMailSender().send(message);
    }
}
