FROM jdk8:sh

ADD target/*.jar app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom" ,"-jar" , "-Xms256m","-Xmx256m" ,"/app.jar"]